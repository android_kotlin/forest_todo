package ch.anmanosca.foresttodo

import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.loader.app.LoaderManager
import androidx.loader.content.AsyncTaskLoader
import androidx.loader.content.Loader
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import ch.anmanosca.foresttodo.data.TaskContract
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<Any> {
    // Constants for logging and referencing to a unique loader
    private val TAG = "MainActivity"
    private val TASK_LOADER_ID = 0

    private var mRecyclerView: RecyclerView? = null

    // Member variables
    private lateinit var mAdapter: CustomCursorAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        try {

            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)

            // Set the RecyclerView to its corresponding view
            mRecyclerView = findViewById(R.id.recyclerViewTasks)

            // Set the layout to a linear layout, which positions
            // the items in the RecyclerView to a linear list
            mRecyclerView!!.layoutManager = LinearLayoutManager(this)

            // Init adapter and attach to recyclerView
            mAdapter = CustomCursorAdapter(this)
            mRecyclerView!!.adapter = mAdapter

            /*
            Add a touch helper to the RecyclerView to recognize swipes when the user wants
            to delete a task. This function uses a callback to signalise such actions.
             */
            ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: ViewHolder,
                    target: ViewHolder
                ): Boolean {
                    return false
                }

                // this method gets called when the user swipes left or right
                override fun onSwiped(
                    viewHolder: ViewHolder,
                    direction: Int
                ) {
                    // use of getTag() to get the id of the swiped item
                    val id = viewHolder.itemView.tag as Int
                    // Build correct URI with the id appended
                    val stringId = id.toString()
                    var uri: Uri = TaskContract().TaskEntry().CONTENT_URI

                    uri = uri.buildUpon().appendEncodedPath(stringId).build()

                    // Delete a single row of data
                    contentResolver.delete(uri, null, null)

                    // Restart the loader to re-query all tasks after deletion
                    LoaderManager.getInstance(this@MainActivity).restartLoader<Any>(
                        TASK_LOADER_ID,
                        null,
                        this@MainActivity
                    )
                }
            }).attachToRecyclerView(mRecyclerView)

            /*
                Assign the Floating Action Button (FAB) to its view and
                attach an OnClickListener to it. When the button is clicked a
                new intent will be created and the AddTaskActivity will be launched
             */
            val fab: FloatingActionButton = findViewById(R.id.fab)

            fab.setOnClickListener {
                val thread: Thread = object : Thread() {
                    override fun run() {
                        try {
                            // Create the new intent to start the AddTaskActivity
                            val addTaskIntent = Intent(this@MainActivity, AddTaskActivity::class.java)
                            startActivity(addTaskIntent)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                }
                thread.start()
            }
           } catch ( e: Exception) {
            e.printStackTrace()
        }
    }

    /*
    This method usually gets called when this activity was paused or restarted.
    Normally this happens after new data  has been inserted in the AddTaskActivity
    and then this method restarts the loader to re-query the data for new changes.
     */
    override fun onResume() {
        super.onResume()
        // re-query for all tasks
        LoaderManager.getInstance(this@MainActivity).restartLoader(TASK_LOADER_ID, null, this)
    }

    /**
     * This method instantiates and returns a new AsyncTaskLoader with the given Id.
     * It will return the task data as a Cursor or null if an error occurs.
     *
     * It also implements the required callbacks at all stages of loading.
     *
     */
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Any> {
        return object : AsyncTaskLoader<Any>(this) {
            // Initialize a Cursor, this will hold all the task data
            var mTaskData: Cursor? = null

            // onStartLoading() is called when a loader first starts loading data
            override fun onStartLoading() {
                if (mTaskData != null) {
                    // Delivers any previously loaded data immediately
                    deliverResult(mTaskData)
                } else {
                    // Force a new load
                    forceLoad()
                }
            }

            // loadInBackground() performs asynchronous loading of data
            override fun loadInBackground(): Cursor? {
                // Will implement to load data

                // Query and load all task data in the background; sort by priority
                // [Hint] use a try/catch block to catch any errors in loading data
                return contentResolver?.query(
                    TaskContract().TaskEntry().CONTENT_URI,
                    null,
                    null,
                    null,
                    TaskContract().TaskEntry().COLUMN_PRIORITY
                )
            }

            // deliverResult sends the result of the load, a Cursor, to the registered listener
            override fun deliverResult(data: Any?) {
                mTaskData = data as? Cursor
                super.deliverResult(data)
            }
        }
    }

    /*
    Called when the loader has finished to load.
     */
    override fun onLoadFinished(loader: Loader<Any>, data: Any?) {
        // Update the adapter data which is used to create ViewHolders
        mAdapter.swapCursor(data as? Cursor)
    }

    /*
     Called when the previous loader is reset. It removes any references
     this activity had.
     */
    override fun onLoaderReset(loader: Loader<Any>) {
        mAdapter.swapCursor(null)
    }
}
