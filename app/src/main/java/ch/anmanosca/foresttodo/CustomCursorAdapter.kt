package ch.anmanosca.foresttodo

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.BaseColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import ch.anmanosca.foresttodo.data.TaskContentProvider
import ch.anmanosca.foresttodo.data.TaskContract
import kotlin.properties.Delegates

class TaskViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var taskDescriptionView: TextView? = null
    var priorityView: TextView? = null

    init {
        taskDescriptionView = itemView.findViewById(R.id.taskDescription)
        priorityView = itemView.findViewById(R.id.priorityTextView)

        try {
            itemView.setOnLongClickListener {
                val p = layoutPosition
                val id = itemView.tag as Int
                // Build correct URI with the id appended
                val stringId = id.toString()

                println("LongClick: $p, $stringId")

                val b = Bundle()
                b.putString("id", stringId)

                try {
                    // Create the new intent to start the AddTaskActivity
                    val addTaskIntent = Intent(itemView.context, AddTaskActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtras(b)
                    itemView.context.startActivity(addTaskIntent)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                true
            }

        }  catch ( e: Exception) {
            e.printStackTrace()
        }
    }

}

/**
 * Creates and binds ViewHolders to a RecyclerView to display data.
 */
class CustomCursorAdapter(context: Context): RecyclerView.Adapter<TaskViewHolder>() {

    private val TAG = "CustomCursorAdapter"
    // Fields for the cursor
    private var mCursor: Cursor? = null
    private var mContext: Context? = null

    init {
        mContext = context
    }

    /*
    Called when ViewHolders are created to fill a RecyclerView.
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TaskViewHolder {
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.task_layout, parent, false)

        return TaskViewHolder(view)
    }

    /*
    Returns the number of tasks
     */
    override fun getItemCount(): Int {
        if (mCursor == null) {
            return 0
        }
        return mCursor!!.count
    }

    /*
    Called to display data at a specified position
     */
    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        // Indices for the _id, description, and priority columns
        val idIndex = mCursor!!.getColumnIndex(BaseColumns._ID)
        val descriptionIndex =
            mCursor!!.getColumnIndex(TaskContract().TaskEntry().COLUMN_DESCRIPTION)
        val priorityIndex = mCursor!!.getColumnIndex(TaskContract().TaskEntry().COLUMN_PRIORITY)

        mCursor?.moveToPosition(position) // get to the right location in the cursor

        // Determine the values of the wanted data
        val id = mCursor!!.getInt(idIndex)
        val description = mCursor!!.getString(descriptionIndex)
        val priority = mCursor!!.getInt(priorityIndex)

        //Set values
        holder.itemView.tag = id
        holder.taskDescriptionView!!.text = description

        // Programmatically set the text and color for the priority TextView
        val priorityString = "" + priority // converts int to String
        holder.priorityView!!.text = priorityString

        val priorityCircle =
            holder.priorityView!!.background as GradientDrawable
        // Get the appropriate background color based on the priority
        val priorityColor: Int = getPriorityColor(priority)
        priorityCircle.setColor(priorityColor)

    }

    /*
    This method swaps the cursor with a new cursor after a re-query
     */
    fun swapCursor(c: Cursor?): Cursor? {
        // check if this cursor is the same as the old
        if (mCursor === c) {
            return null // nothing changed
        }

        val temp = mCursor
        mCursor = c // new cursor value

        // check if it's a valid cursor and if so then update it
        if (c != null) {
            notifyDataSetChanged()
        }

        return temp
    }


    /*
    Selects the correct color
     */
    private fun getPriorityColor(priority: Int): Int {
        var priorityColor = 0
        when (priority) {
            1 -> priorityColor = ContextCompat.getColor(mContext!!, R.color.materialGreen)
            2 -> priorityColor = ContextCompat.getColor(mContext!!, R.color.materialViolett)
            3 -> priorityColor = ContextCompat.getColor(mContext!!, R.color.materialBlue)
            else -> {
            }
        }
        return priorityColor
    }

}