package ch.anmanosca.foresttodo.data

import android.net.Uri
import android.provider.BaseColumns

class TaskContract {

    // which content provider to access
    val AUTHORITY = "ch.anmanosca.foresttodo"

    // Base content URI = "content://" + <AUTHORITY>
    val BASE_CONTENT_URI: Uri = Uri.parse("content://$AUTHORITY")

    // path to the "tasks" directory
    val PATH_TASKS = "tasks"

    /* TaskEntry is an inner class which defines the contents of the task table */
    open inner class TaskEntry : BaseColumns {
        // TaskEntry content URI = base content URI + path
        val CONTENT_URI: Uri =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_TASKS).build()

        // Task table and column names
        val TABLE_NAME = "tasks"

        // TaskEntry implements "BaseColumns" which automatically produces "_ID" column,
        // but we need two other columns in addition
        val COLUMN_DESCRIPTION = "description"
        val COLUMN_PRIORITY = "priority"

    }
}