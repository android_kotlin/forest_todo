package ch.anmanosca.foresttodo.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class TaskDbHelper(context: Context, DATABASE_NAME: String = "taskDB.db", VERSION: Int = 1)
        : SQLiteOpenHelper(context, DATABASE_NAME, null, VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        // Creates the task table (SQL Formatting rules need to be followed!)
        val CREATE_TABLE = "CREATE TABLE " + TaskContract().TaskEntry().TABLE_NAME + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY, " +
                TaskContract().TaskEntry().COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                TaskContract().TaskEntry().COLUMN_PRIORITY + " INTEGER NOT NULL);"

        db?.execSQL(CREATE_TABLE)
    }

    /*
    Discards old table and calls onCreate to create a new one. This only happens
    when the version of this DB is incremented.
     */
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + TaskContract().TaskEntry().TABLE_NAME)
        onCreate(db)
    }
}