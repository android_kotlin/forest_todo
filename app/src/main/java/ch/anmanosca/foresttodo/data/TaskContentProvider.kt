package ch.anmanosca.foresttodo.data

import android.content.*
import android.database.Cursor
import android.database.SQLException
import android.net.Uri


class TaskContentProvider: ContentProvider() {

    // Define final integer constants for the directory of tasks and a single item.
    // It's convention to use 100, 200, 300, etc for directories,
    // and related ints (101, 102, ..) for items in that directory.
    val TASKS = 100
    val TASK_WITH_ID = 101

    // Static variable for the Uri matcher
    private val sUriMatcher: UriMatcher? = buildUriMatcher()

    /*
    Initialize new matcher object without any matches then add matches
     */
    fun buildUriMatcher(): UriMatcher? {
        // Init uriMatcher with no matches (NO_MATCH)
        val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        /*
          All paths added to the UriMatcher have a corresponding int.
          For each kind of uri you may want to access, add the corresponding match with addURI.
          The two calls below add matches for the task directory and a single item by ID.
         */
        uriMatcher.addURI(TaskContract().AUTHORITY, TaskContract().PATH_TASKS, TASKS)
        uriMatcher.addURI(TaskContract().AUTHORITY, TaskContract().PATH_TASKS + "/#", TASK_WITH_ID)

        return uriMatcher
    }


    // Member for TaskDBHelper
    private var mTaskDbHelper: TaskDbHelper? = null

    override fun onCreate(): Boolean {
        val context: Context? = context
        mTaskDbHelper = TaskDbHelper(context as Context)
        return false
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        // Get access to DB
        val db = mTaskDbHelper!!.writableDatabase

        // Write URI matching code to identify the match for the tasks
        val match = sUriMatcher!!.match(uri)
        val returnUri: Uri

        returnUri = when (match) {
            TASKS -> {
                // insert new values into the DB
                val id = db.insert(TaskContract().TaskEntry().TABLE_NAME, null, values)
                if (id > 0) {
                    ContentUris.withAppendedId(TaskContract().TaskEntry().CONTENT_URI, id)
                } else {
                    throw SQLException("Failed to insert row into $uri")
                }
            }
            else -> throw UnsupportedOperationException("Unknown uri: $uri")
        }

        // Notify the resolver if the Uri has changed and return new Uri
        context!!.contentResolver.notifyChange(uri, null)

        return returnUri
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        // Get access to underlying database (read-only for query)
        val db = mTaskDbHelper!!.readableDatabase

        // Write URI match code and set a variable to return a Cursor
        val match = sUriMatcher!!.match(uri)
        var retCursor: Cursor? = null

        // Query for the tasks directory and write a default case
         retCursor = when (match) {
            TASKS -> db.query(
                TaskContract().TaskEntry().TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
            )
             TASK_WITH_ID -> {
                 // Get the task ID from the URI path
                 val id = uri.pathSegments[1]
                 // Use selections/selectionArgs to filter for this ID
                 db.query(
                     true,
                     TaskContract().TaskEntry().TABLE_NAME,
                     arrayOf(TaskContract().TaskEntry().COLUMN_DESCRIPTION,
                         TaskContract().TaskEntry().COLUMN_PRIORITY),
                     "_id=?",
                     arrayOf(id),
                     null,
                     null,
                     null,
                     null
                 )
             }
            else -> throw java.lang.UnsupportedOperationException("Unknown uri: $uri")
        }


        // Set a notification URI on the Cursor and return that Cursor
        retCursor?.setNotificationUri(context!!.contentResolver, uri)

        // Return the desired Cursor
        return retCursor
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        val db = mTaskDbHelper!!.writableDatabase

        val match = sUriMatcher!!.match(uri)
        val tasksUpdated: Int

        tasksUpdated = when (match) {
            TASK_WITH_ID -> {
                val id = uri.pathSegments[1]
                db.update(
                    TaskContract().TaskEntry().TABLE_NAME,
                    values,
                    "_id=?",
                    arrayOf(id)
                )
            }
            else -> throw java.lang.UnsupportedOperationException("Unknown uri: $uri")
        }

        if (tasksUpdated != 0) {
            context!!.contentResolver.notifyChange(uri, null)
        }

        return tasksUpdated
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        // Get access to the database and write URI matching code to recognize a single item
        val db = mTaskDbHelper!!.writableDatabase

        val match = sUriMatcher!!.match(uri)
        // Keep track of the number of deleted tasks
        val tasksDeleted: Int // starts as 0


        // Write the code to delete a single row of data
        // [Hint] Use selections to delete an item by its row ID
        tasksDeleted = when (match) {
            TASK_WITH_ID -> {
                // Get the task ID from the URI path
                val id = uri.pathSegments[1]
                // Use selections/selectionArgs to filter for this ID
                db.delete(
                    TaskContract().TaskEntry().TABLE_NAME,
                    "_id=?",
                    arrayOf(id)
                )
            }
            else -> throw java.lang.UnsupportedOperationException("Unknown uri: $uri")
        }

        // Notify the resolver of a change and return the number of items deleted
        if (tasksDeleted != 0) {
            // A task was deleted, set notification
            context!!.contentResolver.notifyChange(uri, null)
        }

        // Return the number of tasks deleted
        return tasksDeleted
    }

    override fun getType(uri: Uri): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}