package ch.anmanosca.foresttodo

import android.content.ContentValues
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import ch.anmanosca.foresttodo.data.TaskContract
import android.widget.RadioButton
import android.widget.TextView
import kotlin.properties.Delegates

class AddTaskActivity: AppCompatActivity() {

    // members
    private var mPriority = 0
    private var isNewTask = false
    lateinit var taskId: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)

        val id = intent.extras?.getString("id")

        if (id !== null) {
            getTaskById(id)
            taskId = id
            (findViewById<TextView>(R.id.textView)).text = getString(R.string.edit_task_activity_name)
            (findViewById<Button>(R.id.addButton)).text = getString(R.string.update_button)
        } else {
            println("ID: $id")

            // Set highest Priority as default (mPriority = 1)
            findViewById<RadioButton>(R.id.radButton1).isChecked = true
            mPriority = 1
            isNewTask = true
        }
    }

    fun taskUpsertHandler(view: View) {
        if (isNewTask) {
            onClickAddTask(view)
        } else {
            onClickUpdateTask(view)
        }
    }

    /*
    Is called whenever a priority button is clicked and changes the value of mPriority
     */
    fun onPrioritySelected(view: View) {
        when {
            (findViewById<RadioButton>(R.id.radButton1)).isChecked -> {
                mPriority = 1
            }
            (findViewById<RadioButton>(R.id.radButton2)).isChecked -> {
                mPriority = 2
            }
            (findViewById<RadioButton>(R.id.radButton3)).isChecked -> {
                mPriority = 3
            }
        }
    }

        /*
     This method is executed when  the "ADD" button is clicked. It get the
     user input and inserts it into the DB.
      */
    private fun onClickAddTask(view: View) {
        // Check if the EditText is empty, if not retrieve input and store it in an Object
        // NO entry is needed if input is empty
        val input =
            (findViewById<EditText>(R.id.editTextTaskDescription)).text.toString()

        if (input.isEmpty()) {
            return
        }

        // Insert new task data via a ContentResolver
        // Create new empty ContentValues object
        val contentValues = ContentValues()
        // put the retrieved data (Description, Priority) into the contentValues
        contentValues.put(TaskContract().TaskEntry().COLUMN_DESCRIPTION, input)
        contentValues.put(TaskContract().TaskEntry().COLUMN_PRIORITY, mPriority)

        // Insert the content values with a contentResolver
        contentResolver.insert(TaskContract().TaskEntry().CONTENT_URI, contentValues)

        // IMPORTANT! when the activity is finished return to to the MainActivity
        finish()
    }

    private fun onClickUpdateTask(view: View) {
        val input =
            (findViewById<EditText>(R.id.editTextTaskDescription)).text.toString()

        if (input.isEmpty()) {
            return
        }
        val contentValues = ContentValues()
        contentValues.put(TaskContract().TaskEntry().COLUMN_DESCRIPTION, input)
        contentValues.put(TaskContract().TaskEntry().COLUMN_PRIORITY, mPriority)

        var uri: Uri = TaskContract().TaskEntry().CONTENT_URI.buildUpon().appendEncodedPath(taskId).build()
        contentResolver.update(uri, contentValues, null, null)

        finish()
    }

    private fun getTaskById(id: String) {
            var uri: Uri = TaskContract().TaskEntry().CONTENT_URI
            uri = uri.buildUpon().appendEncodedPath(id).build()

            val cursor = contentResolver?.query(
                uri, null, null, null, null
            )

            lateinit var str: String
            var cursorid by Delegates.notNull<Int>()
            var priority by Delegates.notNull<Int>()

            if (cursor!!.moveToFirst()) {
                str = cursor.getString(cursor.getColumnIndex(TaskContract().TaskEntry().COLUMN_DESCRIPTION))
                cursorid = cursor.getInt(cursor.getColumnIndex(TaskContract().TaskEntry().COLUMN_DESCRIPTION))
                priority = cursor.getInt(cursor.getColumnIndex(TaskContract().TaskEntry().COLUMN_PRIORITY))
            }

            (findViewById<EditText>(R.id.editTextTaskDescription)).setText(str)
            setPriorityById(priority)
            println("query1: $str, $cursorid, $priority")

            cursor.close()
    }

    private fun setPriorityById(priority: Int) {
        when (priority) {
            1 -> {
                findViewById<RadioButton>(R.id.radButton1).isChecked = true
            }
            2 -> {
                findViewById<RadioButton>(R.id.radButton2).isChecked = true
            }
            3 -> {
                findViewById<RadioButton>(R.id.radButton3).isChecked = true
            }
        }
        mPriority = priority
    }

}